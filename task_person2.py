from datetime import date
class Person:
    surname = ''
    firstname = ''
    nickname = ''
    birth_day = date.today()

    def __init__(self, sn, fn, dat, nn=None):
        self.surname = sn
        self.firstname = fn
        self.nickname = nn
        self.birth_day = date(dat[0], dat[1], dat[2])

    def get_age(self):
        age = date.today() - self.birth_day
        return int(age.days / 365)

    def get_fullname(self):
        return self.firstname + ' ' + self.surname


p1 = Person("Андрій", "Бондарець", (1999, 11, 17), 'Bond')
p2 = Person("Катерина", "Медвідь", (1999, 10, 21))
print("%s\t|\t%s\t|\t%s|\t%s" % ("Прізвище та ім'я", "Дата народження", "Повних років", "Нік"))
print('_' * 70)
print(
    "%s\t|\t\t%s\t|\t\t%i\t\t|\t%s" % (p1.get_fullname(), p1.birth_day.strftime("%Y/%m/%d"), p1.get_age(), p1.nickname))
print(
    "%s\t|\t\t%s\t|\t\t%i\t\t|\t%s" % (p2.get_fullname(), p2.birth_day.strftime("%Y/%m/%d"), p2.get_age(), p2.nickname))
print()
print(p1.get_fullname() + " на сьогодняшний день (" + date.today().strftime("%Y/%m/%d") + ") має повних " + str(
    p1.get_age()) + " років.")
print("%s на сьогодняшній день (%s) має повних %i років" % (
p2.get_fullname(), date.today().strftime("%Y/%m/%d"), p2.get_age()))
