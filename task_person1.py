# Реалізувати клас Person, який відображає запис в низі контактів
# Клас має 4 атрибути:
# - surname - рядок - прізвище контакту (обов'язковий)
# - firstname - рядок - ім'я контакту(обов'язковий)
# - nickname - рядок - псевдонім контакту(опціональний)
# - bith_date  - об'єкт datatime.date - дата народження контактної особи (обов'язковий)
# Кожен виклик класу повинен створювати екземпляр класу із вкзанаими атрибутами.
# Крім того, є 2 методи:
# - get_age() - повертає вік контактної особи в повних роках на дату виклику і повертає рядок виду: "27"
# - get_fullname() - повертає рядок, що містить повне ім'я (прізвище+ім'я) контакта
from datetime import date
from datetime import date
class Person:
    # surname = ''
    # firstname = ''
    # nickname = ''
    # birth_date = date.today()

    def __init__(self, sn, fn, dat, nn=None):
        self.surname = sn
        self.firstname = fn
        self.nickname = nn
        try:
            y = int(dat[:4])
            m = int(dat[5:7])
            d = int(dat[-2:])
        except:
            print('Задайте правильну дату у форматі YYYY/MM/DD')
        self.birth_date = date(y,m,d)

    def get_age(self):
        age = date.today() - self.birth_date
        return int(age.days / 365)

    def get_fullname(self):
        return self.firstname + ' ' + self.surname

p1 = Person("Андрій", "Бондарець", "1999/11/17", 'Bond')
p2 = Person("Катерина", "Медвідь", "1999/10/21")
p3 = Person("Анастасія", "Бабенко", "1989/03/01")

print("%s\t|\t%s\t|\t%s|\t%s"%("Прізвище та ім'я", "Дата народження", "Повних років", "Нік"))
print('_'*70)
print("%s\t|\t\t%s\t|\t\t%i\t\t|\t%s"%(p1.get_fullname(), p1.birth_date.strftime("%Y/%m/%d"), p1.get_age(), p1.nickname))
print("%s\t|\t\t%s\t|\t\t%i\t\t|\t%s"%(p2.get_fullname(), p2.birth_date.strftime("%Y/%m/%d"), p2.get_age(), p2.nickname))
print("%s\t|\t\t%s\t|\t\t%i\t\t|\t%s"%(p3.get_fullname(), p3.birth_date.strftime("%Y/%m/%d"), p3.get_age(), p3.nickname))
print()
print(p1.get_fullname() + " на сьогодняшний день ("+date.today().strftime("%Y/%m/%d")+") має повних " + str(p1.get_age()) + " років.")
print("%s на сьогодняшній день (%s) має повних %i років"%(p2.get_fullname(), date.today().strftime("%Y/%m/%d"), p2.get_age()))
print("%s на сьогодняшній день (%s) має повних %i років"%(p3.get_fullname(), date.today().strftime("%Y/%m/%d"), p3.get_age()))



