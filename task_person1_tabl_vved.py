# Реалізувати клас Person, який відображає запис в низі контактів
# Клас має 4 атрибути:
# - surname - рядок - прізвище контакту (обов'язковий)
# - firstname - рядок - ім'я контакту(обов'язковий)
# - nickname - рядок - псевдонім контакту(опціональний)
# - bith_date  - об'єкт datatime.date - дата народження контактної особи (обов'язковий)
# Кожен виклик класу повинен створювати екземпляр класу із вкзанаими атрибутами.
# Крім того, є 2 методи:
# - get_age() - повертає вік контактної особи в повних роках на дату виклику і повертає рядок виду: "27"
# - get_fullname() - повертає рядок, що містить повне ім'я (прізвище+ім'я) контакта
from datetime import date
class Person:
    surname = ''
    firstname = ''
    nickname = ''
    birth_date = date.today()

    def __init__(self, sn, fn, dat, nn=None):
        self.surname = sn
        self.firstname = fn
        self.nickname = nn
        self.birth_date = date(dat[0], dat[1], dat[2])

    def get_age(self):
        age = date.today() - self.birth_date
        return int(age.days / 365)

    def get_fullname(self):
        return self.firstname + " " + self.surname


# Введення контактних даних
a = ''
i = 0
p = []
while a != 'N':
    sname = input("Введіть прізвище:\n")
    fname = input("Введіть ім'я:\n")
    nname = input("Введіть нік, якщо відсутній натисніть '-':\n")
    bdate = input("Введіть дату народження у форматі 'DD/MM/YYYY':\n")
    try:
        d = int(bdate[:2])
        m = int(bdate[3:5])
        y = int(bdate[-4:])
    except:
        print('Задайте правильну дату у форматі DD/MM/YYYY')

    p.append(Person(fname, sname, (y, m, d), nname))
    i += 1
    a = input("Добавити новий контакт (Y / N)?\n")
    while a not in ['N', 'Y']:
        a = input("Добавити новий контакт (Y / N)?\n")

# Виведення даних про студентів у вигляді таблиці
i = 0
print("%s\t|\t%s\t|\t%s\t|\t%s" % ("Прізвище та ім'я", "Дата народження", "Повних років", "Нік"))
print('_' * 80)
print(len(p))
while i != len(p):
    st1 = p[i].get_fullname() + ' ' * (20 - len(p[i].get_fullname()))
    st2 = ' ' * 5 + p[i].birth_date.strftime("%d/%m/%Y") + ' ' * 4
    st3 = ' ' * 9 + str(p[i].get_age()) + ' ' * 8
    st4 = ' ' * 3 + p[i].nickname
    print("%s|%s|%s|%s" % (st1, st2, st3, st4))
    i += 1
